import 'package:flutter/material.dart';

class Radar extends StatelessWidget {
  const Radar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text(
        'Radar',
        style: TextStyle(
            fontFamily: 'OpenSans', fontWeight: FontWeight.w700, fontSize: 30),
      ),
    );
  }
}
