import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:hello_weather/view/views.dart';
import 'forecast_cast_hour_card.dart';

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}

WeatherModel weatherModel = WeatherModel();

class Forecast extends StatefulWidget {
  const Forecast({Key? key}) : super(key: key);

  @override
  State<Forecast> createState() => _ForecastState();
}

class _ForecastState extends State<Forecast> {
  @override
  void initState() {
    super.initState();
    getWeatherData();
  }

  OpenWeatherService openWeatherService =
      OpenWeatherService(lat: latG, lon: lonG);
  OpenForeCastWeather openForeCastWeather =
      OpenForeCastWeather(lat: latG, lon: lonG);

  List<Widget> _forecastList = [];

  List<WeatherModel> forecastModel = [];

  bool chartLoaded = true;
  List<String> images = [
    'images/01n.png',
    'images/01d.png',
    'images/02d.png',
    'images/02n.png',
    'images/03d.png',
    'images/03n.png',
    'images/04d.png',
    'images/04n.png',
    'images/09d.png',
    'images/09n.png',
    'images/10d.png',
    'images/10n.png',
    'images/11d.png',
    'images/11n.png',
  ];

  Random random = Random();
  // method to get weather data received by request
  getWeatherData() async {
    var weatherData = await openWeatherService.getWeather();
    setState(() {
      if (weatherData == null) {
        weatherModel.temp = currentTemp;
        weatherModel.feelsLike = currentTemp;
        weatherModel.city = 'Unidentified city';
        weatherModel.tempName = 'Unknown';
        weatherModel.tempDesc = 'Unknown';
        weatherModel.icon = 'snow';
      } else {
        weatherModel.temp = weatherData['main']['temp'];
        weatherModel.feelsLike = weatherData['main']['feels_like'];
        weatherModel.city = weatherData['name'] + '';
        weatherModel.tempName = weatherData['weather'][0]['main'];
        weatherModel.tempDesc = weatherData['weather'][0]['description'];
        weatherModel.icon = weatherData['weather'][0]['icon'];
        currentTemp = weatherData['main']['temp'];
      }
    });
  }

  // builder card forecast data
  List<Widget> _buildChartForecast() {
    _forecastList.clear();
    int i = 0;
    for (i = 0; i < 8; i++) {
      _forecastList.add(
        ForeCastHourCard(
          image: images[random.nextInt(images.length)],
          time: '${i + 1}pm',
          tempForecast: random.nextInt(20) + 15,
        ),
      );
    }

    return _forecastList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 0),
        child: ListView(children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  weatherModel.city != null
                      ? Text(
                          weatherModel.city.toString(),
                          style: const TextStyle(
                              fontSize: 16.0,
                              fontFamily: 'OpenSans',
                              color: colorApp,
                              fontWeight: FontWeight.w100),
                        )
                      : spinkitText,
                  Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                        icon: const Icon(
                          Icons.share,
                          color: colorApp,
                          size: 25,
                        ),
                        padding: const EdgeInsets.all(0),
                        onPressed: () {}),
                  ),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: SizedBox(
              width: double.infinity,
              height: 1.0,
              child: Container(
                color: const Color.fromRGBO(25, 191, 107, 1),
                height: 1.0,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(children: [
              CustomListTile(
                title: 'Right now',
                subtitle:
                    '${weatherModel.tempDesc.toString().capitalize()} for the hour',
                loading: true,
              ),
              Temp(
                temp: weatherModel.temp?.toInt(),
                feelsLike: weatherModel.feelsLike?.toInt(),
                icon: weatherModel.icon,
              ),
              const CustomListTile(
                title: 'Lorem ipsum',
                subtitle:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae.',
                loading: false,
              ),
              !chartLoaded
                  ? Center(child: spinkit)
                  : Container(
                      margin: const EdgeInsets.symmetric(horizontal: 15),
                      color: Colors.white,
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.end,
                        runAlignment: WrapAlignment.end,
                        alignment: WrapAlignment.spaceBetween,
                        children: _buildChartForecast(),
                      ),
                    ),
              CustomListTile(
                title: 'Latitude: $latG',
                subtitle: 'Longitude:$lonG',
                loading: false,
              ),

              //ListView(
              //     scrollDirection: Axis.horizontal,
              //     children: _getForecastList()),
            ]),
          )
        ]),
      ),
    );
  }
}

class Temp extends StatelessWidget {
  const Temp(
      {Key? key,
      required this.temp,
      required this.feelsLike,
      required this.icon})
      : super(key: key);
  final temp, feelsLike, icon;

  @override
  Widget build(BuildContext context) {
    return (temp != null && feelsLike != null && icon != null)
        ? Container(
            color: Colors.white,
            padding: null,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    child: CircleAvatar(
                      radius: 70,
                      backgroundColor: Colors.transparent,
                      child: Image.asset('images/$icon.png'),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '$temp°',
                          style: const TextStyle(
                            fontSize: 56,
                            fontFamily: 'OpenSans',
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          'Feels like $feelsLike°',
                          style: const TextStyle(
                            fontSize: 15,
                            fontFamily: 'OpenSans',
                            color: Color.fromRGBO(91, 107, 119, 1),
                            fontWeight: FontWeight.w400,
                            letterSpacing: 0.3,
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  )
                ]),
          )
        : spinkit;
  }
}

class CustomListTile extends StatelessWidget {
  const CustomListTile(
      {Key? key, required this.title, required this.subtitle, this.loading})
      : super(key: key);

  final String title;
  final String subtitle;
  final bool? loading;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        child: ListTile(
          title: Text(
            title,
            style: const TextStyle(
                fontSize: 30,
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.w600),
          ),
          subtitle: (weatherModel.tempDesc != null || loading == false)
              ? Text(
                  subtitle,
                  style: const TextStyle(
                      color: Colors.black,
                      letterSpacing: 0.5,
                      fontSize: 15,
                      fontFamily: 'OpenSans',
                      fontWeight: FontWeight.w100),
                )
              : spinkitText,
        ),
      ),
    );
  }
}
