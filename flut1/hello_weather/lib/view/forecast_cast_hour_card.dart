import 'package:flutter/material.dart';
import 'package:hello_weather/view/views.dart';

// dynamic card of forecast bar

class ForeCastHourCard extends StatelessWidget {
  const ForeCastHourCard({
    Key? key,
    required this.image,
    required this.time,
    required this.tempForecast,
  }) : super(key: key);

  final image;
  final time;
  final double tempForecast;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.end, children: [
      Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
        height: (tempForecast / currentTemp) * 140,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4), color: colorApp),
        child: Text(
          '${tempForecast.toInt().toString()}°',
          style: const TextStyle(
              color: Colors.white,
              fontSize: 11,
              fontFamily: "OpenSans",
              fontWeight: FontWeight.normal),
        ),
      ),
      Container(
        padding: const EdgeInsets.symmetric(vertical: 3),
        child: Text(
          time.toString(),
          style: const TextStyle(fontSize: 10),
        ),
      ),
      SizedBox(
        width: 22,
        height: 22,
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          child: Image.asset(image),
        ),
      )
    ]);
  }
}
