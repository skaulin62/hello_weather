import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:hello_weather/view/views.dart';

class Location extends StatefulWidget {
  const Location({Key? key}) : super(key: key);

  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location> {
  final _latController = TextEditingController();
  final _lonController = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.all(50),
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  children: const [
                    Icon(
                      Icons.location_pin,
                      color: colorApp,
                      semanticLabel: 'Lon',
                      size: 80,
                    ),
                    Padding(
                      padding: EdgeInsets.all(30),
                      child: Text(
                        'Location',
                        style: TextStyle(
                            fontSize: 30,
                            fontFamily: 'OpenSans',
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    CustomTextField(
                      text: 'Latitude',
                      controller: _latController,
                    ),
                    const SizedBox(height: 30),
                    CustomTextField(
                      text: 'Longitude',
                      controller: _lonController,
                    ),
                    const SizedBox(height: 30),
                    Container(
                      width: 300,
                      height: 50,
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            if (_latController.text.isEmpty ||
                                _lonController.text.isEmpty) {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: const Text(
                                        'Incorrect',
                                        style: TextStyle(
                                          fontFamily: "OpenSans",
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      content: const Text(
                                        'Input correct value',
                                        style: TextStyle(
                                          color: Color.fromARGB(125, 0, 0, 0),
                                        ),
                                      ),
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      actions: [
                                        OutlinedButton(
                                          onPressed: () {
                                            Navigator.pop(context, false);
                                          },
                                          child: const Padding(
                                            padding: EdgeInsets.all(10),
                                            child: Text(
                                              'OK',
                                              style: TextStyle(
                                                color: colorApp,
                                                fontFamily: "OpenSans",
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    );
                                  });
                            } else {
                              latG = double.parse(_latController.text);
                              lonG = double.parse(_lonController.text);
                            }
                          });
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(colorApp)),
                        child: const Text(
                          'Check',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontFamily: 'OpenSans',
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  const CustomTextField(
      {Key? key, required this.text, required this.controller})
      : super(key: key);
  final text;
  final controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      child: TextField(
        decoration: InputDecoration(
          label: Text(text),
          border: const OutlineInputBorder(),
          counterText: '',
        ),
        controller: controller,
        keyboardType: TextInputType.number,
        maxLength: 3,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      ),
    );
  }
}
