import 'package:flutter/material.dart';
import 'package:hello_weather/view/views.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool lights = false;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 40),
            child: Text(
              'Settings',
              style: TextStyle(
                  fontSize: 30,
                  fontFamily: 'OpenSans',
                  fontWeight: FontWeight.bold),
            ),
          ),
          SwitchListTile(
            title: const Text('Dark theme'),
            value: lights,
            activeColor: colorApp,
            onChanged: (bool value) {
              setState(() {
                lights = value;
              });
            },
            secondary: const Icon(
              Icons.lightbulb_rounded,
              color: colorApp,
            ),
          )
        ],
      ),
    );
  }
}
