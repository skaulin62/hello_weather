import 'package:flutter/material.dart';
import 'package:hello_weather/view/views.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static const List<Widget> _views = <Widget>[
    Location(),
    Forecast(),
    Radar(),
    Settings()
  ];

  int _currentIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: _views.elementAt(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentIndex,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.place),
                label: 'Location',
                backgroundColor: Color.fromRGBO(25, 191, 107, 255)),
            BottomNavigationBarItem(
                icon: Icon(Icons.bar_chart),
                label: 'Forecast',
                backgroundColor: Color.fromRGBO(25, 191, 107, 0)),
            BottomNavigationBarItem(
                icon: Icon(Icons.radar),
                label: 'Radar',
                backgroundColor: Color.fromRGBO(25, 191, 107, 255)),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                label: 'Settings',
                backgroundColor: Color.fromRGBO(25, 191, 107, 0))
          ],
          selectedItemColor: const Color.fromRGBO(25, 191, 107, 1),
          unselectedItemColor: const Color.fromRGBO(91, 107, 119, 1),
          selectedFontSize: 13.0,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          }),
      // bottomNavigationBar: BottomNavigationBar(
      //   type: BottomNavigationBarType.fixed,
      //   items: const <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.location_city),
      //       label: 'Location',

      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.graphic_eq),
      //       label: 'Forecast',
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.radar),
      //       label: 'Radar',
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.settings),
      //       label: 'Settings',
      //       backgroundColor: Colors.amber,
      //     ),
      //   ],
      //   selectedItemColor: const Color.fromRGBO(25, 191, 107, 0),
      //   unselectedItemColor: const Color.fromRGBO(91, 107, 119,255),
      //   onTap: null ,
      // ),
    );
  }
}
