import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart' as spin;

export 'package:hello_weather/view/forecast.dart';
export 'package:hello_weather/view/location.dart';
export 'package:hello_weather/view/radar.dart';
export 'package:hello_weather/view/settings.dart';
export 'package:hello_weather/model/weather_model.dart';
export 'package:hello_weather/services/api_weather_data.dart';

// In case the request did not return a value temp, use for calculations chart forecast
double currentTemp = 25;
// location: lat, lot
double latG = 54.99;
double lonG = 73.36;

// color application
const Color colorApp = Color.fromRGBO(25, 191, 107, 1);

// loading widget

Widget spinkit = Container(
  padding: const EdgeInsets.only(top: 10),
  width: double.infinity,
  child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        SizedBox(
          width: 30,
        ),
        spin.SpinKitFadingCircle(
          color: colorApp,
          size: 50,
        )
      ]),
);

Widget spinkitText = Container(
  padding: const EdgeInsets.symmetric(vertical: 5),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: const [
      spin.SpinKitThreeBounce(
        color: colorApp,
        size: 15,
      ),
    ],
  ),
);
