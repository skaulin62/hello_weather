import 'package:http/http.dart' as http;
import 'dart:convert';

const apiUrlWeather = 'https://api.openweathermap.org/data/2.5/weather';
const apiUrlForeCast = 'https://api.openweathermap.org/data/2.5/forecast';
const apiKey = '260a2a0b3dda086d40c03faaa8e92fb1';

// b1b35bba8b434a28a0be2a3e1071ae5b
class ApiWeatherData {
  ApiWeatherData(this.url);
  final String url;

  Future getData() async {
    http.Response response;
    try {
      response =
          await http.get(Uri.parse(url)).timeout(const Duration(seconds: 15));
    } on Exception catch (_) {
      return null;
    }

    if (response.statusCode == 200) {
      String data = response.body;
      return jsonDecode(data);
    } else {
      print(response.body);
      return null;
    }
  }
}

class OpenWeatherService {
  final double lat;
  final double lon;

  OpenWeatherService({required this.lat, required this.lon});

  Future<dynamic> getWeather() async {
    ApiWeatherData apiWeatherData = ApiWeatherData(
        '$apiUrlWeather?lat=$lat&lon=$lon&appid=$apiKey&units=metric&mode=json');
    var weatherData = await apiWeatherData.getData();
    return weatherData;
  }
}

class OpenForeCastWeather {
  final double lat;
  final double lon;

  OpenForeCastWeather({required this.lat, required this.lon});

  Future<dynamic> getWeather() async {
    ApiWeatherData apiWeatherData = ApiWeatherData(
        '$apiUrlForeCast?lat=$lat&lon=$lon&appid=$apiKey&units=metric');
    var weatherData = await apiWeatherData.getData();

    return weatherData;
  }
}
