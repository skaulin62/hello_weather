class WeatherModel {
  double? temp;
  double? feelsLike;
  String? city;
  String? tempName;
  String? tempDesc;
  String? icon;
}
